---
title: "Manual Until It Hurts"
date: 2020-11-17T20:23:54Z
publishdate: 2020-11-17T20:23:54Z
author: Julian Leyh
tags:
  - IndieWeb
  - Hugo
  - Webmentions
  - Syndication
categories:
  - Notes
syndication:
  - title: Twitter
    icon: fab fa-twitter
    url: 'https://twitter.com/Rommudoh/status/1328799452388339714'
  - title: Mastodon
    icon: fab fa-mastodon
    url: 'https://social.tchncs.de/@rommudoh/105227587226736784'
replies:
  - title: Welcome to Webmentions, Julian.
    url: http://superkuh.com/welcome-to-webmentions-julian.html
    by:
      name: superkuh
      url: http://superkuh.com/
  - title: Liked
    url: https://www.jvt.me/mf2/2020/11/7thfh/
    by:
      name: Jamie Tanna
      url: https://www.jvt.me/
---
= Manual Until It Hurts
:Author: Julian Leyh
:Email: julian@vgai.de

A simple setup for webmentions: just a Nginx server that logs the data, like described here: http://superkuh.com/blog/2020-01-10-1.html

For now, I will review and add them manually to the posts.

Also, I added syndication links.